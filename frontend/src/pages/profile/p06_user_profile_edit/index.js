import React, { Component } from 'react';
import Header from '../../../components/header/index';
import Footer from '../../../components/footer';
import UserData from '../../../components/c03_user_data/index';
import ReviewComponent from '../../../components/c04_review_component/index';
import CommentComponent from '../../../components/c06_comment_component/index';
import RestaurantComponent from '../../../components/c07_restaurant_component/index';
import ButtonOrangeXL from '../../../components/button_orange_xl/index';
import EditComponent from '../../../components/c08_edit_component/index';
import './index.scss';
import { useEffect, useState } from 'react';

const UserProfileEdit = () => {
    // const [state, setState] = useState(initialState);
    const [username, setUsername] = useState('username');
    const [firstName, setFirstName] = useState('first_name');
    const [lastName, setLastName] = useState('last_name');
    const [email, setEmail] = useState('email');
    const [location, setLocation] = useState('location');
    const [phone, setPhone] = useState('phone');
    const [thingsILove, setThingsILove] = useState('thingsILove');
    const [description, setDescription] = useState('description');

    useEffect(() => {
        // console.log('component did mount....');
        const token = localStorage.getItem('token');

        const url = "https://luna-team-3.propulsion-learn.ch/backend/api/me/";
        const method = "GET";
        // const headers = new Headers({ 'Content-type': 'application/json' });
        const headers = new Headers({ 'Authorization': `Bearer ${token}` });

        const config = { method, headers };

        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setFirstName(data.first_name);
                    setLastName(data.last_name);
                    setUsername(data.username);
                    setEmail(data.email);
                    setLocation(data.location);
                    setPhone(data.phone);
                    setThingsILove(data.things_i_love);
                    setDescription(data.profile_description);

                    console.log(data);
                }
            })
            .catch(reject => console.log(reject));
    });

    return (
        <div className='p06-user-profile'>
            <Header active='profile'/>
            <div className='p06-upper-photo'>
            </div>
            <div className='p06-main'>
                <div className='p06-left-part'>
                    <UserData activeRow='edit_profile' />
                </div>
                <div className='p06-middle-part'>
                    <div className='p06-user-profile-top'>
                        <div className='p06-name'>{username}</div>
                        <div className='p06-row'>{location}</div>
                        <div className='p06-row'>6 reviews</div>
                        <div className='p06-row'>210 comments</div>
                    </div>
                    <p className='p06-header'>edit userprofile</p>
                    <div className='p06-user-profile-bottom'>
                        <ul className='p06-items'>
                            <li className='p06-item'>< EditComponent email={email} /></li>
                        </ul>
                        
                    </div>
                    <div className='p06-button-container'>
                        {/* <ButtonOrangeXL label='Create Restaurant' /> */}
                    </div>
                </div>
                <div className='p06-right-part'>
                    <p className='p06-rp-name'>about {firstName} {lastName}</p>
                    <div className='p06-rp-attribute'>
                        <p className='p06-rp-label'>location</p>
                        <p className='p06-rp-value'>{location}</p>
                    </div>
                    <div className='p06-rp-attribute'>
                        <p className='p06-rp-label'>Luna member since</p>
                        <p className='p06-rp-value'>April, 2018</p>
                    </div>
                    <div className='p06-rp-attribute'>
                        <p className='p06-rp-label'>Things I love</p>
                        <p className='p06-rp-value'>{thingsILove}</p>
                    </div>
                    <div className='p06-rp-attribute'>
                        <p className='p06-rp-label'>Description</p>
                        <p className='p06-rp-value'>{description}</p>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default UserProfileEdit;