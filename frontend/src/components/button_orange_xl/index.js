import React, { Component } from 'react';
import "./index.scss";

const ButtonOrangeXL = (props) => {
    return (
        <div className='button-orange-xl' onClick={props.action}>
            {props.label ? props.label : 'DEFAULT'}
        </div>
    );
}

export default ButtonOrangeXL;