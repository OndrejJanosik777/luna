import React, { Component, } from 'react';
import RatingComponent from '../c05_rating_component/index';
import ButtonOrangeXL from '../button_orange_xl/index';
import { useNavigate } from 'react-router-dom';
import './index.scss';
import { useEffect, useState } from 'react';

const EditComponent = (props) => {
    let navigate = useNavigate();

    const [username, setUsername] = useState('username');
    const [firstName, setFirstName] = useState('first_name');
    const [lastName, setLastName] = useState('last_name');
    const [email, setEmail] = useState('email');
    const [location, setLocation] = useState('location');
    const [phone, setPhone] = useState('phone');
    const [thingsILove, setThingsILove] = useState('thingsILove');
    const [description, setDescription] = useState('description');

    useEffect(() => {
        // console.log('component did mount....');
        const token = localStorage.getItem('token');

        const url = "https://luna-team-3.propulsion-learn.ch/backend/api/me/";
        const method = "GET";
        // const headers = new Headers({ 'Content-type': 'application/json' });
        const headers = new Headers({ 'Authorization': `Bearer ${token}` });

        const config = { method, headers };

        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setFirstName(data.first_name);
                    setLastName(data.last_name);
                    setUsername(data.username);
                    setEmail(data.email);
                    setLocation(data.location);
                    setPhone(data.phone);
                    setThingsILove(data.things_i_love);
                    setDescription(data.profile_description);

                    console.log(data);
                }
            })
            .catch(reject => console.log(reject));
    }, []);

    let action = () => {
        console.log('saving profile...');

        const token = localStorage.getItem('token');

        const url = "https://luna-team-3.propulsion-learn.ch/backend/api/me/";
        const method = "PATCH";

        let body = {
            first_name: firstName,
            last_name: lastName,
            email: email,
            location: location,
            phone: phone,
            things_i_love: thingsILove,
            profile_description: description,
            username: username,
        }

        body = JSON.stringify(body);
        const headers = new Headers({ 'Authorization': `Bearer ${token}` });
        const config = { method, headers, body };

        fetch(url, config)
            .then(response => {response.json()
                if(response.status === 200){
                    // dispatch({type:"setUserInfo",payload:body});
                    // localStorage.setItem("user",body);
                    // navigate("/");
                }else if(response.status !== 200){
                    return alert("Please confirm your inputs");
                }
            })
            .catch(reject => console.log(reject));
    }

    return ( 
        <div className='c08-edit-component'>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Username</p>
                {/* e => setEmail(e.target.value) */}
                <input className='c08-input-text-50' type='text' value={username} onChange={e => setUsername(e.target.value)} />
                <p className='c08-validation-error'>{ username.length === 0 ? 'This field is required' : ''}</p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>First name</p>
                <input className='c08-input-text-50' type='text' value={firstName} onChange={e => setFirstName(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Last name</p>
                <input className='c08-input-text-50' type='text' value={lastName} onChange={e => setLastName(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>E-Mail</p>
                <input className='c08-input-text-50' type='text' value={email} onChange={e => setEmail(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Location</p>
                <input className='c08-input-text-50' type='text' value={location} onChange={e => setLocation(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Phone</p>
                <input className='c08-input-text-50' type='text' value={phone} onChange={e => setPhone(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Things I love</p>
                <input className='c08-input-text-100' type='text' value={thingsILove} onChange={e => setThingsILove(e.target.value)} />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-input-field-small'>
                <p className='c08-edit-input-label'>Description</p>
                <textarea 
                    className='c08-input-textarea-100' 
                    type='textfield' 
                    value={description} 
                    onChange={e => setDescription(e.target.value)} 
                    rows="5" cols="33"
                />
                <p className='c08-validation-error'></p>
            </div>
            <div className='c08-footer'>
                <ButtonOrangeXL label='Save' action={() => action()} />
                {/* <Link value='Delete account' className='c08-link-delete' /> */}
                <div 
                    className='c08-link-delete'
                    onClick={() => navigate('/')}
                >Delete account</div>
            </div>
        </div>
     );
}

export default EditComponent;